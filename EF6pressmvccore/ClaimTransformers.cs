﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EF6pressmvccore.Pages.Shared;
using EF6pressmvccore.Properties;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.IIS;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EF6pressmvccore
{
    public class ClaimsTransformer : IClaimsTransformation
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPrincipal _principal;
        public ClaimsTransformer(IUnitOfWork unitOfWork, IPrincipal principal)
        {
            _unitOfWork = unitOfWork;
            _principal = principal;
        }
        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            var currentPrincipal = (ClaimsIdentity)_principal.Identity;
            var identity = (ClaimsIdentity)principal.Identity;
            if (currentPrincipal.Claims.All(p => p.Type != "UserId"))
            {
                var person = _unitOfWork.PersonRepository.GetPersonBySubjectId(principal.Claims.First(p => p.Type == "sub").Value);
                person.Wait();
                if (person.Result != null)
                {
                    currentPrincipal.AddClaim(new Claim("UserId", person.Result.Id.ToString()));
                    currentPrincipal.AddClaim(new Claim("TenantId", person.Result.PersonTeams.FirstOrDefault(p => p.Team.TeamType == TeamType.OrganizationTeam)?.Team.OrganizationId.ToString()));
                    if (principal.Claims.Any(p => p.Type == "Admin"))
                    {
                        currentPrincipal.AddClaim(new Claim("Admin", "True"));
                    }
                }
                foreach (var claim in identity.Claims)
                {
                    currentPrincipal.AddClaim(claim);
                }
            }
            return Task.FromResult(principal);
        }
    }

}
